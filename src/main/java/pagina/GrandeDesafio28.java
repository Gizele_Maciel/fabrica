package pagina;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class GrandeDesafio28 {
	
	@Test
	public void GrandeDesafio28_8 () {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\GIZELE\\Downloads\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("file:///C:/Users/GIZELE/Desktop/Fabrica%20de%20Software/componentes.html");
		
		WebElement cadastrar = driver.findElement (By.id("elementosForm:cadastrar"));
		cadastrar.click();
		
		Alert alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Nome eh obrigatorio", texto);
		alert.accept();
				
		
		driver.switchTo(). window("");
		
		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		nome.clear();
		nome.sendKeys("Gizele");
		
		WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		sobrenome.clear();
		sobrenome.sendKeys("Maciel");
		
		WebElement sexoF = driver.findElement(By.id("elementosForm:sexo:1"));
		sexoF.click();
				
		WebElement comida = driver.findElement(By.id("elementosForm:comidaFavorita:0"));
		WebElement comida2 = driver.findElement(By.id("elementosForm:comidaFavorita:1"));
		
		comida.click();
		comida2.click();
		
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		combo.selectByValue("superior");
		Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());
		
		WebElement element1 = driver.findElement(By.id("elementosForm:esportes"));
		Select combo1 = new Select(element1);
		combo1.selectByValue("Corrida");
		Assert.assertEquals("Corrida", combo1.getFirstSelectedOption().getText());
		
		WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		sugestoes.clear();
		sugestoes.sendKeys("Muito bom ter aprendido tudo at� agora");
		
		WebElement cadastrar2 = driver.findElement (By.id("elementosForm:cadastrar"));
		cadastrar.click();
		
		driver.quit();
		
	}
	
	}
